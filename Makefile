# SPDX-FileCopyrightText: Peter Pentchev <roam@ringlet.net>
# SPDX-License-Identifier: BSD-2-Clause

PACKAGE?=	sssnips
VERSION?=	0.6.0

SCRIPTS?=	bigfile blprev \
		csort \
		declean devver dpkg-compare \
		getkey git-bcmp git-ss git-ssnoq git-ssq \
		hostsearch \
		mkv \
		noagent \
		pbuilder-gather \
		qdc qidiff qpr \
		resock \
		signfiles sssnips-refresh stun switch-link syspower \
		tmux-reattach \
		vcr vcrtty \
		ws
MAN1?=		bigfile.1.gz blprev.1.gz \
		csort.1.gz \
		declean.1.gz devver.1.gz dpkg-compare.1.gz \
		getkey.1.gz git-bcmp.1.gz git-ss.1.gz git-ssnoq.1.gz git-ssq.1.gz \
		hostsearch.1.gz \
		mkv.1.gz \
		noagent.1.gz \
		pbuilder-gather.1.gz \
		qdc.1.gz qidiff.1.gz qpr.1.gz \
		resock.1.gz \
		signfiles.1.gz sssnips-refresh.1.gz stun.1.gz switch-link.1.gz syspower.1.gz \
		tmux-reattach.1.gz \
		vcr.1.gz vcrtty.1.gz \
		ws.1.gz
LINKS?=		pbuilder-gather:pdebuild-gather \
		pbuilder-gather:debuild-gather

PREFIX?=	/usr
BINDIR?=	${PREFIX}/bin
SHAREDIR?=	${PREFIX}/share
MANDIR?=	${PREFIX}/share/man/man

CP?=		cp
ECHO?=		echo
GZIP?=		gzip -c9
INSTALL?=	install
MKDIR?=		mkdir -p
RM?=		rm -f
LN_S?=		ln -s

BINOWN?=	root
BINGRP?=	root
BINMODE?=	755

SHAREOWN?=	${BINOWN}
SHAREGRP?=	${BINGRP}
SHAREMODE?=	444

COPY?=		-c
STRIP?=		-s
INSTALL_PROGRAM?=	${INSTALL} ${COPY} ${STRIP} -o ${BINOWN} -g ${BINGRP} -m ${BINMODE}
INSTALL_SCRIPT?=	${INSTALL} ${COPY} -o ${BINOWN} -g ${BINGRP} -m ${BINMODE}
INSTALL_DATA?=	${INSTALL} ${COPY} -o ${SHAREOWN} -g ${SHAREGRP} -m ${SHAREMODE}

pkgdir=		${PACKAGE}-${VERSION}
pkgtarball=	${pkgdir}.tar

all:		${SCRIPTS} ${MAN1}

%:		%.sh
		${CP} $< $@

%.1.gz:		%.1
		${GZIP} $< > $@

install:	all
		${MKDIR} ${DESTDIR}${BINDIR}
		${INSTALL_SCRIPT} ${SCRIPTS} ${DESTDIR}${BINDIR}
		${MKDIR} ${DESTDIR}${MANDIR}1
		${INSTALL_DATA} ${MAN1} ${DESTDIR}${MANDIR}1
		for pair in ${LINKS}; do	\
			src=$${pair%%:*};	\
			dst=$${pair#*:};	\
			${LN_S} $$src ${DESTDIR}${BINDIR}/$$dst;	\
			${LN_S} $$src.1.gz ${DESTDIR}${MANDIR}1/$$dst.1.gz;	\
		done

clean:
		${RM} ${SCRIPTS} ${MAN1}

dist:
		@echo "Checking for forgotten development versions:"
		@! sh devver.sh | egrep -e "^"
		rm -rf ${pkgdir} ${pkgtarball}
		mkdir -p ${pkgdir}
		git ls-files -z | xargs -0 tar -cpf - | tar -xpf - -C ${pkgdir}
		tar cf ${pkgtarball} ${pkgdir}/
		gzip -c9 < ${pkgtarball} > ${pkgtarball}.gz
		bzip2 -c9 < ${pkgtarball} > ${pkgtarball}.bz2
		xz -c9 < ${pkgtarball} > ${pkgtarball}.xz

distclean:	clean
		rm -rf ${pkgdir} ${pkgtarball} ${pkgtarball}.gz ${pkgtarball}.bz2 ${pkgtarball}.xz
