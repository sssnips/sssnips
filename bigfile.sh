#!/bin/sh
#
# SPDX-FileCopyrightText: Peter Pentchev <roam@ringlet.net>
# SPDX-License-Identifier: BSD-2-Clause

set -e

usage()
{
	cat <<EOUSAGE
Usage:	bigfile [-v] [-S signal] [-w interval] -p pid -s maxsize filename
	-p	the process ID to kill
	-S	the signal to send; default: TERM
	-s	the file size limit
	-v	verbose operation; display diagnostic messages
	-w	the wait interval in seconds; default: 5
EOUSAGE
}

pid=''
sig='TERM'
size=''
verbose=''
wait='5'
while getopts 'p:S:s:vw:' o; do
	case "$o" in
		p)
			pid=$OPTARG
			;;

		S)
			sig=$OPTARG
			;;

		s)
			size=$OPTARG
			;;

		v)
			verbose=1
			;;

		w)
			wait="$OPTARG"
			;;

		*)
			echo "Invalid option '$o'" 1>&2
			usage 1>&2
			exit 1
			;;
	esac
done

if [ -z "$size" ]; then
	echo 'No maximum size specified' 1>&2
	exit 1
fi
if ! expr "x$size" : 'x[1-9][0-9]*$' > /dev/null; then
	echo 'Invalid maximum size specified' 1>&2
	exit 1
fi
if [ -z "$pid" ]; then
	echo 'No process ID specified' 1>&2
	exit 1
fi
if ! expr "x$pid" : 'x[1-9][0-9]*$' > /dev/null; then
	echo 'Invalid process ID specified' 1>&2
	exit 1
fi
if ! expr "x$wait" : 'x[1-9][0-9]*$' > /dev/null; then
	echo 'Invalid wait interval specified' 1>&2
	exit 1
fi
if [ -z "$sig" ]; then
	echo 'No signal specified' 1>&2
	exit 1
fi
if ! expr "x$sig" : 'x[1-9][0-9]*' > /dev/null; then
	if ! kill -l | fgrep -qwe "$sig"; then
		echo 'Invalid signal specified' 1>&2
		exit 1
	fi
fi

shift `expr $OPTIND - 1`
if [ $# != 1 ]; then
	usage 1>&2
	exit 1
fi
filename="$1"

if [ ! -e "$filename" ]; then
	echo "No such file: $filename" 1>&2
	exit 1
fi

[ -z "$verbose" ] || echo "Checking '$filename' each $wait seconds for size $size pid $pid"
while :; do
	csize=`stat -c '%s' "$filename"`
	[ -z "$verbose" ] || echo "csize is $csize"
	if [ -z "$csize" ] || ! expr "x$csize" : 'x[0-9][0-9]*' > /dev/null; then
		echo "Could not obtain the current size of $filename" 1>&2
		exit 2
	fi
	[ -z "$verbose" ] || echo "Checking size $csize against $size"
	if [ "$csize" -ge "$size" ]; then
		[ -z "$verbose" ] || echo "Reached it, signalling $pid"
		if ! kill -"$sig" "$pid"; then
			echo "Could not send signal $sig to $pid" 1>&2
			exit 1
		fi
		[ -z "$verbose" ] || echo "All fine"
		exit 0
	fi
	[ -z "$verbose" ] || echo "Still not big enough, sleeping"
	sleep "$wait"
done
