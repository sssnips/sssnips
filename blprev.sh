#!/bin/sh
#
# SPDX-FileCopyrightText: Peter Pentchev <roam@ringlet.net>
# SPDX-License-Identifier: BSD-2-Clause

set -e

version()
{
	echo 'blprev 0.1.0'
}

usage()
{
	cat <<EOUSAGE
Usage:	blprev [index]
	blprev -V | -h

	-h	display program usage information and exit
	-V	display program version information and exit

The optional "index" argument specifies which of the previous revisions to
compare with; the default value is 1 meaning the last one.
EOUSAGE
}

unset hflag Vflag

while getopts 'hV' o; do
	case "$o" in
		h)
			hflag=1
			;;

		V)
			Vflag=1
			;;

		*)
			usage 1>&2
			exit 1
			;;
	esac
done
# If -V and/or -h is specified, print the request info and exit.
[ -z "$Vflag" ] || version
[ -z "$hflag" ] || usage
[ -z "$Vflag$hflag" ] || exit 0

shift `expr "$OPTIND" - 1`
index=1
if [ "$#" -gt 1 ]; then
	usage 1>&2
	exit 1
elif [ "$#" -eq 1 ]; then
	if ! expr "x$1" : 'x[1-9][0-9]*$' > /dev/null; then
		echo "Invalid index '$1', should be a positive integer" 1>&2
		usage 1>&2
		exit 1
	fi
	index="$1"
fi

pwd=$PWD
name=`basename "$pwd"`
parent=`dirname "$pwd"`
parentname=`basename "$parent"`
if [ "$parentname" != 'deb' ] && [ "$parentname" != 'attempts' ]; then
	echo 'This tool is supposed to be invoked from a */deb/NN-name directory' 1>&2
	exit 1
fi
if ! expr "x$name" : 'x[0-9][0-9]-.' > /dev/null && \
    ! expr "x$name" : 'x\([A-Za-z][A-Za-z0-9]*\)-[0-9][0-9]-.' > /dev/null; then
	echo 'This tool is supposed to be invoked from a */deb/NN-name directory' 1>&2
	exit 1
fi

tempf=`mktemp blprev-dirs.txt.XXXXXX`
trap "rm -f '$tempf'" EXIT QUIT TERM INT HUP 

find -- "$parent"/ -mindepth 1 -maxdepth 1 -type d | env LANG=C sort | fgrep -x -B "$index" -e "$pwd" > "$tempf"
count=`wc -l "$tempf" | awk '{ print $1 }'`
if [ "$count" != "`expr $index + 1`" ]; then
	echo "Could not determine the previous directory's name!" 1>&2
	echo "Expected previous, then current ('$pwd'), got:" 1>&2
	cat "$tempf" 1>&2
	exit 1
fi
prev=`head -n1 "$tempf"`

printf '===== Lintian:\n\n'
diff -u "$prev"/lintian.txt lintian.txt || true

printf '\n\n===== Build:\n\n'
diff -u "$prev"/*build *build || true
