#!/bin/sh
#
# SPDX-FileCopyrightText: Peter Pentchev <roam@ringlet.net>
# SPDX-License-Identifier: BSD-2-Clause

set -e

version()
{
	echo 'declean 0.1.0'
}

usage()
{
	cat <<EOUSAGE
Usage:	declean [-n]
	declean -h | -V

	-h	display program usage information and exit
	-n	work on a native package, no 'dist' directory
	-V	display program version information and exit
EOUSAGE
}

unset hflag nflag Vflag
while getopts 'hnV' o; do
	case "$o" in
		h)
			hflag=1
			;;

		n)
			nflag=1
			;;

		V)
			Vflag=1
			;;

		*)
			usage 1>&2
			exit 1
			;;
	esac
done
[ -z "$Vflag" ] || version
[ -z "$hflag" ] || usage
[ -z "$Vflag$hflag" ] || exit 0
shift `expr "$OPTIND" - 1`
if [ "$#" -ne 0 ]; then
	usage 1>&2
	exit 1
fi

if [ -z "$nflag" ] && [ ! -d ../dist ]; then
	echo 'No dist directory' 1>&2
	exit 1
fi

pkgname=`dpkg-parsechangelog | awk '/^Source:/ { print $2 }'`
if [ -z "$pkgname" ]; then
	echo 'Could not determine the package name from debian/changelog' 1>&2
	exit 1
fi
pkgver=`dpkg-parsechangelog | sed -nEe '/^Version:/ { s/^Version:[[:space:]]+([^):-]+:)?([^-]+).*/\2/; p; }'`
if [ -z "$pkgver" ]; then
	echo 'Could not determine the package version from debian/changelog' 1>&2
	exit 1
fi

find .. -mindepth 1 -maxdepth 1 \( \( -type f -name '*_*' \) -or \( -type l -name '*_*.build' \) \) | xargs rm -fv
[ -n "$nflag" ] || ln -v ../dist/"$pkgname"_"$pkgver".orig* ../
