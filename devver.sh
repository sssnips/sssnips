#!/bin/sh
#
# SPDX-FileCopyrightText: Peter Pentchev <roam@ringlet.net>
# SPDX-License-Identifier: BSD-2-Clause

set -e

version()
{
	echo 'devver 0.1.0'
}

usage()
{
	cat <<EOUSAGE
Usage:	devver [-- egrep options]
	devver -V | -h

	-h	display program usage information and exit
	-V	display program version information and exit
EOUSAGE
}

unset hflag Vflag

while getopts 'hV' o; do
	case "$o" in
		h)
			hflag=1
			;;

		V)
			Vflag=1
			;;

		*)
			usage 1>&2
			exit 1
			;;
	esac
done
[ -z "$Vflag" ] || version
[ -z "$hflag" ] || usage
[ -z "$Vflag$hflag" ] || exit 0

shift `expr "$OPTIND" - 1`

(if [ -d .git ]; then
	git ls-files -z
else
	find . -type f -print0
fi) | xargs -0 egrep -e '[[:digit:]]+\.[[:digit:]]\.[[:digit:]]([.+]dev|_)[[:digit:]]+' "$@"
