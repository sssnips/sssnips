#!/bin/sh
#
# SPDX-FileCopyrightText: Peter Pentchev <roam@ringlet.net>
# SPDX-License-Identifier: BSD-2-Clause

set -e

version()
{
	echo 'dpkg-compare 0.1.0'
}

usage()
{
	cat <<EOUSAGE
Usage:	dpkg-compare first-dir second-dir
	dpkg-compare -V | -h

	-h	display program usage information and exit
	-V	display program version information and exit
EOUSAGE
}

unset hflag Vflag

while getopts 'hV' o; do
	case "$o" in
		h)
			hflag=1
			;;

		V)
			Vflag=1
			;;

		*)
			usage 1>&2
			exit 1
			;;
	esac
done
[ -z "$Vflag" ] || version
[ -z "$hflag" ] || usage
[ -z "$Vflag$hflag" ] || exit 0

shift `expr "$OPTIND" - 1`
if [ "$#" -ne 2 ]; then
	echo 'No directories specified' 1>&2
	usage 1>&2
	exit 1
fi

p1dir=$1
p2dir=$2

if [ ! -d "$p1dir" ] || [ ! -r "$p1dir" ]; then
	echo "Nonexistent or unreadable directory $p1dir" 1>&2
	exit 1
fi
if [ ! -d "$p2dir" ] || [ ! -r "$p2dir" ]; then
	echo "Nonexistent or unreadable directory $p2dir" 1>&2
	exit 1
fi

tempd=`mktemp -d dpkg-compare.XXXXXX`
trap "rm -rf '$tempd'" EXIT QUIT TERM INT HUP 

# OK, it's really ugly, having to do everything twice, but... well.
# Maybe someday I'll figure out a better way to do it.

t1dir="$tempd/pkg1"
t2dir="$tempd/pkg2"
mkdir "$t1dir" "$t2dir"

p1list="$t1dir/packages.lst"
p2list="$t2dir/packages.lst"
find "$p1dir"/ -mindepth 1 -maxdepth 1 -type f -name '*.deb' -print0 | xargs -0rn1 basename | env LANG=C sort > "$p1list"
find "$p2dir"/ -mindepth 1 -maxdepth 1 -type f -name '*.deb' -print0 | xargs -0rn1 basename | env LANG=C sort > "$p2list"

while read pkg; do
	echo "=== $pkg"
	if ! fgrep -qxe "$pkg" "$p2list"; then
		echo "- not present at all in the second directory"
		continue
	fi

	d1="$t1dir/$pkg"
	d2="$t2dir/$pkg"
	mkdir "$d1" "$d2"

	dpkg-deb -e "$p1dir/$pkg" "$d1"
	dpkg-deb -e "$p2dir/$pkg" "$d2"

	if [ -f "$d1/md5sums" ] && [ -f "$d2/md5sums" ]; then
		env LANG=C sort -k2 "$d1/md5sums" > "$d1/md5sums.sorted"
		env LANG=C sort -k2 "$d2/md5sums" > "$d2/md5sums.sorted"
		diff -u "$d1/md5sums.sorted" "$d2/md5sums.sorted" || true
	else
		mkdir "$d1/full" "$d2/full"
		dpkg-deb -R "$p1dir/$pkg" "$d1/full"
		dpkg-deb -R "$p1dir/$pkg" "$d2/full"
		find -L "$d1/full" -type l -delete
		find -L "$d2/full" -type l -delete
		diff -qr "$d1/full" "$d2/full" || true
	fi
done < "$p1list"
