#!/bin/sh
#
# SPDX-FileCopyrightText: Peter Pentchev <roam@ringlet.net>
# SPDX-License-Identifier: BSD-2-Clause

[ -z "$KEYSERVERS" ] && KEYSERVERS='pool.sks-keyservers.net'
KEYHTTPPROXY=''
[ -z "$NOKEYPROXY" ] && KEYHTTPPROXY="--keyserver-options http-proxy=$HTTP_PROXY"
for srv in $KEYSERVERS; do
	echo "=== Attempting to fetch $* from $srv..."
	gpg --keyserver="$srv" $KEYHTTPPROXY --recv-key $*
done
