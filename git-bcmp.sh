#!/bin/sh
#
# SPDX-FileCopyrightText: Peter Pentchev <roam@ringlet.net>
# SPDX-License-Identifier: BSD-2-Clause

set -e

version()
{
	echo 'git-bcmp 0.1.0'
}

usage()
{
	cat <<EOUSAGE
Usage:	git-bcmp remote
	git-bcmp -V | -h

	-h	display program usage information and exit
	-V	display program version information and exit
EOUSAGE
}

unset hflag Vflag

while getopts 'hVv' o; do
	case "$o" in
		h)
			hflag=1
			;;

		V)
			Vflag=1
			;;

		*)
			usage 1>&2
			exit 1
			;;
	esac
done
[ -z "$Vflag" ] || version
[ -z "$hflag" ] || usage
[ -z "$Vflag$hflag" ] || exit 0

shift `expr "$OPTIND" - 1`
if [ "$#" -ne 1 ]; then
	echo 'No remote name specified' 1>&2
	exit 1
fi
remote=$1
if ! git remote | fgrep -qxe "$remote"; then
	echo "Remote '$remote' not found" 1>&2
	exit 1
fi

br_local=`mktemp git-bcmp-local.txt.XXXXXX`
trap "rm -f '$br_local'" EXIT QUIT TERM INT HUP 
br_remote=`mktemp git-bcmp-remote.txt.XXXXXX`
trap "rm -f '$br_local' '$br_remote'" EXIT QUIT TERM INT HUP 

git branch -v | cut -c 3- | env LANG=C sort > "$br_local"
git branch -r -v | cut -c 3- | \
	sed -ne '/^'"$remote"'\// { s/^'"$remote"'\///; p; }' | \
	env LANG=C sort > "$br_remote"
diff -bu -- "$br_local" "$br_remote"
