#!/bin/sh
#
# SPDX-FileCopyrightText: Peter Pentchev <roam@ringlet.net>
# SPDX-License-Identifier: BSD-2-Clause

set -e

usage()
{
	cat <<EOQ
Usage:	hostsearch [-hV] [-f hostsfile] pkgname...
	-f	specify the hosts list file, default int.hoster.bg.z
	-h	display program usage information and exit
	-V	display program version information and exit
EOQ
}

version()
{
	echo 'hostsearch 0.01'
}

zonefile='int.hoster.bg.z'
hflag=''
Vflag=''
while getopts 'f:hV' o; do
	case "$o" in
		f)
			zonefile="$OPTARG"
			;;
		h)
			hflag=1
			;;
		V)
			Vflag=1
			;;
		*)
			usage 1>&2
			exit 1
			;;
	esac
done

[ -z "$Vflag" ] || version
[ -z "$hflag" ] || usage
[ -z "$hflag$Vflag" ] || exit 0

shift `expr "$OPTIND" - 1`
if [ $# -lt 1 ]; then
	usage 1>&2
	exit 1
fi
pkg="$1"

if [ ! -r "$zonefile" ]; then
	echo "Missing or unreadable zone file '$zonefile'" 1>&2
	exit 1
fi
for i in `cat "$zonefile"`; do
	ssh $v "$i" dpkg -l "$@" 2>&1 | sed -ne '/^i/ { s@^@'"$i"' @; p; }'
done
