#!/bin/sh
#
# SPDX-FileCopyrightText: Peter Pentchev <roam@ringlet.net>
# SPDX-License-Identifier: BSD-2-Clause

set -e

version() {
	echo 'massdeb 0.1.0.dev1'
}

usage() {
	cat <<EOUSAGE
Usage:	massdeb [-v] build
	massdeb [-v] clean
	massdeb [-v] list
	massdeb help
	massdeb version
	massdeb -V | -h
EOUSAGE
}

prep_work() {
	rm -rf -- "$wrkdir"
	mkdir -- "$wrkdir"
	cd -- "$wrkdir"
	mkdir good bad work
	: > packages.txt
	for p in $pkgs; do
		pdir="$debbase/$p/$p/debian"
		if [ -d "$pdir" ]; then
			printf '%s\n' "$p" >> packages.txt
		else
			echo "No Debian directory $pdir" > bad/"$p.log"
		fi
	done
	pkgs="$(cat packages.txt)"
}

list_bad() {
	set +x
	local dump="$1"
	find bad/ -mindepth 1 -maxdepth 1 -type f -name '*.log' | while read pf; do
		p="$(basename -- "$pf")"
		p="$(basename -- "$p" '.log')"
		printf 'BAD\t%s\n' "$p"
		[ -z "$dump" ] || cat -- "$pf"
	done
}

pkg_loop() {
	local cmd="$*"
	for p in $pkgs; do
		logf="work/$p.log"
		res=0
		if (cd -- "$debbase/$p/$p"; $cmd) > "$logf" 2>&1; then
			mv -- "$logf" good/
		else
			mv -- "$logf" bad/
		fi
	done
	list_bad
}

do_declean() {
	if [ -d '../dist' ]; then
		declean
	else
		declean -n
	fi
}

do_sbuild() {
	sbuild ${MASSDEB_CHROOT:+-c "$MASSDEB_CHROOT"}
}

unset hflag Vflag v
while getopts 'hNn:S:s:u:Vv-:' o; do
	case "$o" in
		h)
			hflag=1
			;;

		V)
			Vflag=1
			;;

		v)
			v='-v'
			;;

		-)
			if [ "$OPTARG" = 'help' ]; then
				hflag=1
			elif [ "$OPTARG" = 'version' ]; then
				Vflag=1
			else
				echo "Invalid long option '$OPTARG' specified" 1>&2
				usage 1>&2
				exit 1
			fi
			;;

		*)
			usage 1>&2
			exit 1
			;;
	esac
done
[ -z "$Vflag" ] || version
[ -z "$hflag" ] || usage
[ -z "$Vflag$hflag" ] || exit 0

shift `expr "$OPTIND" - 1`
if [ "$#" != 1 ]; then
	usage 1>&2
	exit 1
fi
cmd="$1"
case "$cmd" in
	help)
		usage
		exit
		;;

	version)
		version
		exit
		;;
esac

[ -z "$v" ] || set -x

debbase='/home/roam/deb'
pkgfile="$debbase/txt/roam-packages.txt"
pkgs="$(fgrep -vwe nomass -- "$pkgfile" | cut -c3- | cut -f1 | sort)"
wrkdir="$debbase/mass"

prep_work
case "$cmd" in
	list)
		set +x
		printf '%s\n' "$pkgs"
		list_bad 1
		;;

	clean)
		pkg_loop do_declean
		;;

	build)
		pkg_loop do_sbuild
		;;

	*)
		usage 1>&2
		exit 1
		;;
esac
