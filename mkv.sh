#!/bin/sh
#
# SPDX-FileCopyrightText: Peter Pentchev <roam@ringlet.net>
# SPDX-License-Identifier: BSD-2-Clause

set -e

usage()
{
	cat <<EOU
Usage:	mkv [-hV] dircomponent [file...]
	-h	display this usage message and exit;
	-V	display program version information and exit.
EOU
}

version()
{
	echo 'mkv 0.02'
}

basedir=''
h_or_V=''
while getopts 'hV' o; do
	case "$o" in
		h)
			usage
			h_or_V=1
			;;
		V)
			version
			h_or_V=1
			;;
		*)
			usage 1>&2
			exit 1
			;;
	esac
done
[ -n "$h_or_V" ] && exit 0

if [ $# -lt 1 ]; then
	usage 1>&2
	exit 1
fi
dircomp="$1"
shift
if [ -z "$dircomp" ] ||
   ! expr "x$dircomp" : 'x[A-Za-z0-9_-]*$' > /dev/null; then
	echo "Invalid directory component name: $dircomp" 1>&2
	exit 1
fi

[ -n "$basedir" ] || basedir="$MKV_BASE"
if [ -z "$basedir" ]; then
	username=''
	for t in "$USER" "$LOGNAME" "`whoami`"; do
		if [ -n "$t" ]; then
			username="$t"
			break
		fi
	done
	if [ -z "$username" ]; then
		echo 'Could not determine your username!' 1>&2
		exit 1
	fi

	homedir=''
	for t in ~ "$HOME" "/home/$username" "/home/$USER" "/home/$LOGNAME"; do
		if [ -d "$t" ] && [ -w "$t" ]; then
			homedir="$t"
			break
		fi
	done
	if [ -z "$homedir" ]; then
		echo 'Could not find a suitable home directory!' 1>&2
		exit 1
	fi

	tmpdir=''
	for t in "$TEMPDIR" "$TEMP" "$TMP" "$HOME/tmp" '/var/tmp' '/tmp'; do
		if [ -d "$t" ] && [ -w "$t" ]; then
			tmpdir="$t"
			break
		fi
	done
	if [ -z "$tmpdir" ]; then
		echo 'Could not find a suitable temporary directory!' 1>&2
		exit 1
	fi

	basedir="$tmpdir/v/$username"
fi

shell=''
for t in "$SHELL" '/bin/sh'; do
	if [ -n "$t" ] && [ -x "$t" ]; then
		shell="$t"
		break
	fi
done
if [ -z "$shell" ]; then
	echo 'Could not determine a shell to run!' 1>&2
	exit 1
fi

mkdir -p -m 700 "$basedir"

dirname="$basedir/$dircomp"
mkdir -m 700 "$dirname"
if [ $# -gt 0 ]; then
	if ! mv "$@" "$dirname"; then
		echo 'Could not move files into the temporary directory' 1>&2
		rm -rf "$dirname"
		exit 1
	fi
fi
cd "$dirname"
"$shell" || true

cat <<EOM

=========== mkv: You seem to be done with the temporary directory

Hit Enter to remove the temporary directory '$dirname', or enter
any non-empty string to let it be:
EOM

read t
if [ -z "$t" ]; then
	cd "$basedir"
	rm -rf "$dirname"
	echo "Successfully removed the temporary directory '$dirname'"
fi
