#!/bin/sh
#
# SPDX-FileCopyrightText: Peter Pentchev <roam@ringlet.net>
# SPDX-License-Identifier: BSD-2-Clause

set -e

version()
{
	echo 'pbuilder-gather 0.1.0'
}

usage()
{
	cat <<EOUSAGE
Usage:	pbuilder-gather [-Nv] [-a arch] outputdir
	pbuilder-gather [-Nv] -C
	pbuilder-gather -V | -h

	-a	specify the package architecture
	-C	clean up; do not gather the files, remove them instead
	-h	display program usage information and exit
	-N	no operation mode; display what would have been done
	-V	display program version information and exit
	-v	verbose operation; display diagnostic output
EOUSAGE
}

pbroot='/var/cache/pbuilder'
pbresult="$pbroot/result"
dresult='..'

arch=`dpkg --print-architecture`
unset clean hflag noop Vflag v

while getopts 'a:ChNVv' o; do
	case "$o" in
		a)
			arch="$OPTARG"
			;;

		C)
			clean=1
			;;

		h)
			hflag=1
			;;

		N)
			noop='echo'
			;;

		V)
			Vflag=1
			;;

		v)
			v='-v'
			;;

		*)
			usage 1>&2
			exit 1
			;;
	esac
done
[ -z "$Vflag" ] || version
[ -z "$hflag" ] || usage
[ -z "$Vflag$hflag" ] || exit 0

shift `expr "$OPTIND" - 1`
if [ -z "$clean" ]; then
	if [ "$#" -ne 1 ]; then
		usage 1>&2
		exit 1
	fi
	outdir=$1
else
	if [ "$#" -ne 0 ]; then
		usage 1>&2
		exit 1
	fi
fi

bn=`basename "$0"`
case "$bn" in
	pbuilder-gather|pbuilder-gather.*|pdebuild-gather|pdebuild-gather.*)
		result="$pbresult"
		;;

	debuild-gather|debuild-gather.*)
		result="$dresult"
		;;

	*)
		echo "Unable to determine the operation mode for '$bn'" 1>&2
		usage 1>&2
		exit 1
		;;
esac

pver=`dpkg-parsechangelog | awk '/^Source:/ { src = $2 } /^Version:/ { ver = $2; sub(/[1-9][0-9]*:/, "", ver); } END { print src "_" ver }'`
pverarch="${pver}_${arch}"
buildlog="../$pverarch.build"
if [ -z "$clean" ] && [ ! -f "$buildlog" ]; then
	echo "Unable to find build log $buildlog for package $pver architecture $arch" 1>&2
	exit 1
fi

changes="$result/$pverarch.changes"
if [ -z "$clean" ] && [ ! -f "$changes" ]; then
	echo "Unable to find changes file $changes for package $pver architecture $arch" 1>&2
	exit 1
fi

if [ -z "$clean" ] && [ ! -d "$outdir" ]; then
	mkdir $v "$outdir"
fi

if [ -z "$clean" ]; then
	$noop cp -p $v "$buildlog" "$changes" "$outdir/"
else
	[ -f "$buildlog" ] && $noop rm $v "$buildlog"
	[ -f "$changes" ] || exit 0
fi

dscresult=`mktemp pbuilder-gather-dscverify.txt.XXXXXX`
trap "rm -f '$dscresult'" EXIT QUIT TERM INT HUP 
filelist=`mktemp pbuilder-gather-packages.txt.XXXXXX`
trap "rm -f '$dscresult' '$filelist'" EXIT QUIT TERM INT HUP 

dscverify -u "$changes" > "$dscresult"
awk '$1 == "validating" { print $2 }' "$dscresult" > "$filelist"

[ -n "$clean" ] && $noop rm $v "$changes"
while read f; do
	if [ -z "$clean" ]; then
		$noop cp -p $v "$result/$f" "$outdir/"
	else
		$noop rm $v "$result/$f"
	fi
done < "$filelist"
