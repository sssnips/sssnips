#!/bin/sh
#
# SPDX-FileCopyrightText: Peter Pentchev <roam@ringlet.net>
# SPDX-License-Identifier: BSD-2-Clause

set -e

version()
{
	echo 'qdc 0.2.1'
}

usage()
{
	cat <<EOUSAGE
Usage:	qdc [-n]
	qdc -h | -V

	-h	display program usage information and exit
	-n	work on a native package, no 'dist' directory
	-V	display program version information and exit
EOUSAGE
}

unset Cflag hflag nflag Vflag
while getopts 'ChnV' o; do
	case "$o" in
		C)
			Cflag=1
			;;

		h)
			hflag=1
			;;

		n)
			nflag='-n'
			;;

		V)
			Vflag=1
			;;

		*)
			usage 1>&2
			exit 1
			;;
	esac
done
[ -z "$Vflag" ] || version
[ -z "$hflag" ] || usage
[ -z "$Vflag$hflag" ] || exit 0
shift `expr "$OPTIND" - 1`
if [ "$#" -ne 0 ]; then
	usage 1>&2
	exit 1
fi

[ -n "$Cflag" ] || debian/rules clean

if [ -f 'debian/patches/series' ]; then
	quilt pop -a || [ "$?" -eq 2 ]
fi

declean $nflag

if [ -d '.git' ]; then
	# If the only modifications to the Git tree are files deleted by
	# the package cleanup, restore them.

	if ! git status --short | egrep -qve '^ D' -e '^[?][?][[:space:]]*\.pc/?$'; then
		git status --short | awk '/^ D/ { print $2 }' | tr "\n" "\0" | xargs -0r git checkout
	fi
fi
