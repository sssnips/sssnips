#!/bin/sh
#
# SPDX-FileCopyrightText: Peter Pentchev <roam@ringlet.net>
# SPDX-License-Identifier: BSD-2-Clause

set -e

qpath=$QUILT_PATCHES
qrc="$HOME/.quiltrc"
if [ -z "$qpath" ] && [ -f "$qrc" ] && [ -r "$qrc" ]; then
	qpath=`sh -c ". \"$qrc\" > /dev/null 2>&1; echo \"\\\$QUILT_PATCHES\"" || true`
fi
if [ -z "$qpath" ]; then
	qpath='patches'
fi
if [ ! -d "$qpath" ]; then
	echo 'Could not find the quilt patches directory' 1>&2
	exit 1
fi

qtop=`quilt top`
if [ -z "$qtop" ]; then
	echo 'Could not query the top quilt patch' 1>&2
	exit 1
fi
qfile="$qpath/$qtop"
if [ ! -f "$qfile" ]; then
	echo "No top patch file $qfile" 1>&2
	exit 1
fi

quilt diff -p ab | interdiff -p1 "$qfile" /dev/stdin
