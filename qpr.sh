#!/bin/sh
#
# SPDX-FileCopyrightText: Peter Pentchev <roam@ringlet.net>
# SPDX-License-Identifier: BSD-2-Clause

echo "http://www.FreeBSD.org/cgi/query-pr.cgi?pr=$1"
