#!/bin/sh
#
# SPDX-FileCopyrightText: Peter Pentchev <roam@ringlet.net>
# SPDX-License-Identifier: BSD-2-Clause

echo 'export SSH_AUTH_SOCK='\'"$(find /tmp -mindepth 1 -maxdepth 1 -type d -name 'ssh-*' -user "`whoami`" -exec find '{}' -mindepth 1 -maxdepth 1 -type s -name 'agent*' -print0 ';' | xargs -r0 ls -1t | head -n1)"\'
