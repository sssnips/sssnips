#!/bin/sh
#
# SPDX-FileCopyrightText: Peter Pentchev <roam@ringlet.net>
# SPDX-License-Identifier: BSD-2-Clause

set -e

mypath="$(readlink -f -- "$0")"
mydir="$(dirname -- "$mypath")"

ruv="$mydir/ruv.sh"
if [ ! -f "$ruv" ] || [ -h "$ruv" ]; then
	echo "Not a regular file: $ruv" 1>&2
	exit 1
fi

find . \
	'(' -type d -name .git -prune ')' \
	-or \
	'(' \
		-type d \
		-exec test -f '{}/pyvenv.cfg' ';' \
		-print0 \
		-prune \
	')' | \
	xargs -0r -I '{}' -- cp -v -- "$ruv" '{}/bin/ruv'
