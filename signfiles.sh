#!/bin/sh
#
# SPDX-FileCopyrightText: Peter Pentchev <roam@ringlet.net>
# SPDX-License-Identifier: BSD-2-Clause

for i in "$@"; do
	echo "===== $i"
	gpg -ab "$i"
done
