#!/bin/sh
#
# SPDX-FileCopyrightText: Peter Pentchev <roam@ringlet.net>
# SPDX-License-Identifier: BSD-2-Clause

set -e

def_homedir=''
[ -n "$HOME" ] && def_homedir="$HOME"
[ -z "$def_homedir" ] && [ -n "$USER" ] && def_homedir="/home/$USER"
[ -z "$def_homedir" ] && [ -n "$LOGNAME" ] && def_homedir="/home/$LOGNAME"
[ -z "$def_homedir" ] && def_homedir=~

def_bindir="$def_homedir/bin"
def_refdir="$def_homedir/lang/sh/misc/sssnips"

version()
{
	echo 'sssnips-refresh 0.1.0'
}

usage()
{
	cat <<EOUSAGE
Usage:	sssnips-refresh [-Nv] [-b bindir] [-r referencedir]
	sssnips-refresh -V | -h

	-b	specify the path to the target directory (default: $def_bindir)
	-h	display program usage information and exit
	-N	no operation mode; display what would have been done
	-r	specify the path to the reference directory (default: $def_refdir)
	-V	display program version information and exit
	-v	verbose operation; display diagnostic output
EOUSAGE
}

unset hflag noop Vflag v
bindir="$def_bindir"
refdir="$def_refdir"

while getopts 'b:hNr:Vv' o; do
	case "$o" in
		b)
			bindir="$OPTARG"
			;;
			
		h)
			hflag=1
			;;

		N)
			noop='echo'
			;;

		r)
			refdir="$OPTARG"
			;;

		V)
			Vflag=1
			;;

		v)
			v='-v'
			;;

		*)
			usage 1>&2
			exit 1
			;;
	esac
done
[ -z "$Vflag" ] || version
[ -z "$hflag" ] || usage
[ -z "$Vflag$hflag" ] || exit 0

shift `expr "$OPTIND" - 1`
if [ "$#" -ne 0 ]; then
	usage 1>&2
	exit 1
fi

if [ ! -d "$bindir" ]; then
	echo "No target directory $bindir" 1>&2
	exit 1
fi
if [ ! -d "$refdir" ]; then
	echo "No reference directory $refdir" 1>&2
	exit 1
fi

find "$refdir"/ -mindepth 1 -maxdepth 1 -type f -name '*.sh' | while read src; do
	if [ ! -x "$src" ]; then
		echo "Skipping non-executable script $src" 1>&2
		continue
	fi

	scr=`basename "$src" .sh`
	dst="$bindir/$scr"
	if [ -e "$dst" ]; then
		if [ ! -x "$dst" ]; then
			echo "Skipping non-executable bindir script $dst" 1>&2
			continue
		fi
		if ! cmp -s "$src" "$dst"; then
			echo "$dst exists, but it's not the same as $src" 1>&2
		fi
	else
		oldIFS="$IFS"
		IFS=':'
		unset next
		for d in $PATH; do
			tst="$d/$scr"
			[ -x "$tst" ] || continue
			if [ "$tst" != "$dst" ]; then
				next="$tst"
				break
			fi
		done
		IFS="$oldIFS"

		if [ -z "$next" ] || ! cmp -s "$src" "$next"; then
			$noop ln -s $v "$src" "$dst"
		fi
	fi
done
