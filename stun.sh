#!/bin/sh
#
# SPDX-FileCopyrightText: Peter Pentchev <roam@ringlet.net>
# SPDX-License-Identifier: BSD-2-Clause

usage() {
	cat <<'EOUSAGE'
Usage:	stun [-Anst] hostname [hostname...]
	stun -h | -V | --features | --help | --version

	-A	do not autodetect whether to forward the SSH agent for the first host
	-h	display program usage information and exit
	-n	start a shell on the last host
	-s	start screen on the last host
	-t	start tmux on the last host (default)
	-V	display program version information and exit

	--features
		display a list of supported program features and exit
EOUSAGE
}

get_command() {
	local cmd='' agent='' first='1'
	while [ "$#" -gt 0 ]; do
		if [ -n "$first" ] && [ -z "$noautofwd" ]; then
			agent=''
		elif printf -- '%s' "$STUN_NO_AGENT" | grep -qFwe "$1"; then
			agent='-a'
		else
			agent='-A'
		fi
		unset first
		cmd="${cmd}ssh -t $agent $1 "
		shift
	done
	echo "$cmd"
}

: "${STUN_SESS:=stun}"

stun_version='0.3.0'

unset hflag Vflag features noautofwd
mode='tmux'

while getopts 'AhnstV-:' o; do
	case "$o" in
		A)
			noautofwd=1
			;;

		h)
			hflag=1
			;;

		n)
			mode='none'
			;;

		s)
			mode='screen'
			;;

		t)
			mode='tmux'
			;;

		V)
			Vflag=1
			;;

		-)
			case "$OPTARG" in
				features)
					features=1
					;;

				help)
					hflag=1
					;;

				version)
					Vflag=1
					;;

				*)
					echo "Unrecognized long option '$OPTARG'" 1>&2
					usage 1>&2
					exit 1
					;;
			esac
			;;

		*)
			usage 1>&2
			exit 1
			;;
	esac
done
shift "$((OPTIND - 1))"

[ -z "$Vflag" ] || echo "stun $stun_version"
[ -z "$hflag" ] || usage
[ -z "$features" ] || echo "Features: stun=$stun_version"
[ -z "$Vflag$hflag$features" ] || exit 0

if [ "$#" = "0" ]; then
	usage 1>&2
	exit 1
fi

cmd=`get_command "$@"`

case "$mode" in
	none)
		$cmd
		;;

	screen)
		$cmd screen -DR
		;;

	tmux)
		# Isn't escaping a whole lot of fun...
		$cmd 'if [ -n "$SSH_AUTH_SOCK" ]; then
			stun_cache_dir="${XDG_CACHE_HOME-$HOME/.cache}/tmux-sock";
			stun_cache_ssh_sock="$stun_cache_dir/ssh-"'"'$STUN_SESS'"'".sock";
			mkdir -p -- "$stun_cache_dir"
			ln -sf -- "$SSH_AUTH_SOCK" "$stun_cache_ssh_sock";
			export SSH_AUTH_SOCK="$stun_cache_ssh_sock";
		fi;
		if tmux list-windows -t '"'$STUN_SESS'"' > /dev/null 2>&1; then
			tmux attach-session -d -t '"'$STUN_SESS'"';
		else
			tmux new-session -s '"'$STUN_SESS'"';
		fi'
		;;

	*)
		echo "stun internal error: weird mode '$mode'" 1>&2
		exit 1
		;;
esac
