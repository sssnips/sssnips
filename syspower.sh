#!/bin/sh
#
# SPDX-FileCopyrightText: Peter Pentchev <roam@ringlet.net>
# SPDX-License-Identifier: BSD-2-Clause

set -e

basedir='/sys/class/power_supply'
pattern='BAT'

if [ ! -d "$basedir" ]; then
	echo "No power supply base: $basedir" 1>&2
	exit 1
fi
cd "$basedir"
sub=`ls -d "$pattern"*`
if [ -z "$sub" ]; then
	echo "No batteries" 1>&2
	exit 1
fi

echo "Processing batteries: $sub"
for dev in $sub; do
	echo "Device $dev"
	fnow="$dev/charge_now"
	ffull="$dev/charge_full"
	if [ ! -r "$fnow" ] || [ ! -r "$ffull" ]; then
		echo "Unreadable $fnow or $ffull" 1>&2
		continue;
	fi
	cnow=`cat "$fnow"`
	cfull=`cat "$ffull"`
	if [ -z "$cnow" ]; then
		echo "Could not read the current charge from $fnow" 1>&2
		continue
	fi
	if ! expr "x$cnow" : 'x[1-9][0-9]*$' > /dev/null; then
		echo "Not a number in the current charge file $fnow" 1>&2 1>&2
		continue
	fi
	if [ -z "$cfull" ]; then
		echo "Could not read the full charge from $ffull" 1>&2
		continue
	fi
	if ! expr "x$cfull" : 'x[1-9][0-9]*$' > /dev/null; then
		echo "Not a number in the full charge file $ffull" 1>&2 1>&2
		continue
	fi
	if [ "$cfull" = 0 ]; then
		echo "Weirdness, zero full charge in $ffull" 1>&2
		continue
	fi

	printf '%s\t%d\t%d\t%.2f\n' "$dev" "$cnow" "$cfull" `expr "$cnow" \* 100 / "$cfull"`
done
