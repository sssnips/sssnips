#!/bin/sh
#
# SPDX-FileCopyrightText: Peter Pentchev <roam@ringlet.net>
# SPDX-License-Identifier: BSD-2-Clause

set -e

if tmux has-session 2>/dev/null; then
	exec tmux attach-session -d
else
	exec tmux new-session
fi
