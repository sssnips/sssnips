#!/bin/sh
#
# SPDX-FileCopyrightText: Peter Pentchev <roam@ringlet.net>
# SPDX-License-Identifier: BSD-2-Clause

set -e

usage()
{
	cat <<EOUSAGE
Usage:	vcr [-k] [-f filename] [-t tty]
	vcr [-h | -V]

	-f	specify the definition file to read
	-h	display program usage information and exit
	-k	keep going even if some of the commands exit with an error
	-t	specify the terminal to run commands for
	-V	display program version information and exit
	-X	do not wait for confirmation in an X session
EOUSAGE
}

version()
{
	echo 'vcr 0.02'
}

display_commands()
{
	while [ "$#" -gt 0 ]; do
		echo "$1"
		shift
	done
}

run_command()
{
	if ! expr "x$1" : 'x.*"' > /dev/null &&
	    ! expr "x$1" : "x.*'" > /dev/null; then
		$vcr_exec $1
	else
		$vcr_exec sh -c "$1"
	fi
}

unset clearerr filename hflag Vflag Xflag
while getopts 'f:hkt:VX' o; do
	case "$o" in
		f)
			filename="$OPTARG"
			;;
		h)
			hflag=1
			;;
		k)
			clearerr=1
			;;
		t)
			TTY="$OPTARG"
			;;
		V)
			Vflag=1
			;;
		X)
			Xflag=1
			;;
		*)
			usage 1>&2
			exit 1
			;;
	esac
done
[ -z "$Vflag" ] || version
[ -z "$hflag" ] || usage
[ -z "$hflag$Vflag" ] || exit 0

# Try vcrtty(1) first
if [ -z "$TTY" ]; then
	vcrtty=`which vcrtty || true`
	if [ -n "$vcrtty" ]; then
		TTY=`"$vcrtty"`
	fi
fi
if [ -z "$TTY" ]; then
	TTY=`tty`
fi
if [ -z "$TTY" ]; then
	echo "Where are you?" 1>&2
	exit 1
fi

# Look for a definition file if not specified
if [ -z "$filename" ] && [ -e '/etc/vcr' ]; then
	filename='/etc/vcr'
fi
if [ -z "$filename" ] && [ -n "$HOME" ] && [ -d "$HOME" ] && \
   [ -e "$HOME/.vcr" ]; then
	filename="$HOME/.vcr"
fi

if [ -z "$filename" ]; then
	echo "No definition file" 1>&2
	exit 1
fi
if [ ! -r "$filename" ]; then
	echo "Unreadable definition file $filename" 1>&2
	exit 1
fi
set --
exec 3<"$filename"
while read ctty cmd <&3; do
	if [ "$ctty" = "$TTY" ]; then
		set -- "$@" "$cmd"
	fi
done
exec 3<&-
if [ "$#" = 0 ]; then
	echo "Don't know what to do on tty $TTY!" 1>&2
	exit 1
fi

if [ -z "$Xflag" ] || ! expr "x$TTY" : 'xxdesktop' > /dev/null ; then
	echo 'Press Enter to execute'
	display_commands "$@"
	echo '...or enter anything to skip:'
	read foo
else
	# Running under X and the -X option is specified
	foo=''
fi
if [ -z "$foo" ]; then
	set -e
	[ -n "$clearerr" ] && set +e
	while [ "$#" -gt 0 ]; do
		if [ "$#" = 1 ]; then
			vcr_exec='exec'
			run_command "$1"
		else
			cmd="$1"
			vcr_exec=''
			if expr "x$cmd" : 'x-' > /dev/null; then
				cmd=${cmd#-}
				run_command "$cmd" || true
			else
				run_command "$cmd"
			fi
		fi
		shift
	done
	echo 'We should not be here after the last exec, should we?!' 1>&2
	exit 1
else
	echo 'Command execution skipped'
fi
