#!/bin/sh
#
# SPDX-FileCopyrightText: Peter Pentchev <roam@ringlet.net>
# SPDX-License-Identifier: BSD-2-Clause

set -e

# Try tty(1) first
if [ -z "$TTY" ]; then
	if tty > /dev/null 2>&1; then
		TTY=`tty`
	fi
fi

# If not, let's see if we're on an X desktop
if [ -z "$TTY" ] && [ -n "$DISPLAY" ]; then
	# Can we even tell?
	wmctrl=`which wmctrl || true`
	if [ -n "$wmctrl" ]; then
		TTY=`"$wmctrl" -d | awk '$2 == "*" {print "xdesktop" $1}'`
	else
		# Nah, just pretend we're on desktop 0
		TTY='xdesktop0'
	fi
fi

# If not... well, just give up already :)
[ -z "$TTY" ] || echo "$TTY"
