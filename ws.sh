#!/bin/sh
#
# SPDX-FileCopyrightText: Peter Pentchev <roam@ringlet.net>
# SPDX-License-Identifier: BSD-2-Clause

set -e

header='1'
opts=''
while getopts 'husfVo' o; do
	if [ "$o" = '?' ]; then
		w '-?' 2>&1 | sed -ne '1!p' 1>&2
		exit 1
	fi
	if [ "$o" = 'h' ]; then
		header=''
	fi
	opts="$opts$o"
done
shift `expr "$OPTIND" - 1`

tempfile=`mktemp -t ws.XXXXXX`
if [ -z "$tempfile" ] || [ ! -f "$tempfile" ]; then
	echo 'Could not create a temporary file' 1>&2
	exit 1
fi
trap "rm -f -- \"$tempfile\"" EXIT HUP INT QUIT TERM
if [ ! -r "$tempfile" ] || [ ! -w "$tempfile" ]; then
	echo 'Unusable temporary file' 1>&2
	exit 1
fi

if [ -z "$COLUMNS" ]; then
	# Okay then, we assume we are running on a GNU/Linux system (else why
	# would there be any need for a 'sorted w'? :) BSD systems do it
	# by default :) ...so we assume a GNU-style stty with "columns 105"
	# instead of "105 columns".  It's trivial to add a second s/// to
	# the sed(1) invocation, and I will do that if anybody requests it.
	#
	cols=`stty -a | sed -e 's/.*columns \([1-9][0-9]*\).*/\1/; q'`
	if [ -n "$cols" ]; then
		COLUMNS="$cols"
		export COLUMNS
	fi
fi

w ${opts:+"-$opts"} | while read line; do
	if [ -n "$header" ]; then
		echo "$line"
		if expr "x$line" : '^xUSER' > /dev/null; then
			header=''
		fi
	else
		echo "$line" >> "$tempfile"
	fi
done

# Bah, it would be nice to have some combination of alpha- and numeric
# sorting, so that tty1 and tty10 are not lumped together before tty2.
sort -k2,2 -- "$tempfile"
